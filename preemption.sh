K="${1:-microk8s.kubectl}"

$K delete job/low-priority-job -n playground 2>/dev/null
$K delete job/high-priority-job -n playground 2>/dev/null
$K apply -f init.yaml
$K create -f low-priority-job.yaml
sleep 1
$K create -f high-priority-job.yaml

# echo 'waiting for both job to complete...'
# while : ; do
#     status_low=$($K get job low-priority-job --no-headers -o custom-columns=":status.conditions[0].status" -n playground)
#     status_high=$($K get job high-priority-job --no-headers -o custom-columns=":status.conditions[0].status" -n playground)
#     if [ "$status_low" = "True" ] || [ "$status_high" = "True" ]; then break; fi
# done

# echo "low-priority-job:"
# $K describe job.batch/low-priority-job -n playground | grep -A 5 Events
# echo -e "\nhigh-priority-job:"
# $K describe job.batch/high-priority-job -n playground | grep -A 5 Events

watch microk8s.kubectl get all -n playground

