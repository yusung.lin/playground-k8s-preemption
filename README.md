# Overview

This is a simple preemption playground.

```bash
./preemption.sh
```

Before getting started, check your memory and cpu resources and adjust resources requests accordingly in each YAML to trigger preemption.

```bash
free -h
lscpu
```

